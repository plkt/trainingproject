<cfdocument format="PDF">
	<cfoutput>
		<cfdocumentitem type="header">
		    <h1 style="text-align:center;">BIG FANCY HEADER</h1>
		</cfdocumentitem>
		Bacon ipsum dolor sit amet sirloin fatback #dateformat(now(), "short")#
		<cfdocumentitem type="pagebreak"/>
		Bacon ipsum dolor sit amet sirloin fatback #dateformat(now(), "short")#
		<cfdocumentitem type="footer">
		    <h1 style="text-align:center;">Page #cfdocument.currentPageNumber# of #cfdocument.totalPageCount#</h1>
		</cfdocumentitem>
	</cfoutput>
</cfdocument>