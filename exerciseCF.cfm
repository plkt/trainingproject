This is a test page<br><br>

<cfset greeting = "pulkit">
	<cfoutput>
		Hello #greeting#
		<cfdump var = "#greeting#" />
	</cfoutput><br><hr>
	
	

<cfscript>
	myString = "That's a simple code";
	myStringLen = Len(myString);
	WriteOutput(myStringLen );
</cfscript><br><hr>



<cfscript>
	myStruct={};
	myStruct.key1="pulkit";//using dot notation
	myStruct["key2"]="paul";//using associative array notation
	WriteOutput(mystruct.key1 & " ");//pulkit	
	WriteOutput(mystruct.key2);//paul
</cfscript><br><hr>



<cfinclude template="header.cfm">
	<p> 
		WELCOME this my website 
	</p>
<cfinclude template="footer.cfm"><hr>




<cfset firstName="Barney">
	<cfparam name="firstName" default="Ozzy">
		<cfoutput>
			Hello #firstName#
		</cfoutput><br><hr>		
		



<cfif IsDefined("a")>
	Hello #a#!
<cfelse>
		Hello Stranger
</cfif><br><hr>



<cfset DaysSinceTurnOfCentury = DateDiff("d", "25/06/2019", now() ) />
	<cfoutput>#DaysSinceTurnOfCentury#</cfoutput>
<hr>



<cfset twoDaysFromNow = DateAdd("d", now(), 2 ) />
<cfoutput>#twoDaysFromNow#</cfoutput>
<hr>


<cfset DateToday = now() />
	<cfoutput>
		#DateToday#
	</cfoutput>
<cfdump var = "#DateToday#" /><br><hr>


<p>Mixing date with string</p>
<cfset DateToday = "Today is: #now()#" />
	<cfdump var = "#DateToday#" /><br><hr>




<cfset DateToday = "Today is: #now()#" />
	
	<cfdump var = "#DateToday#" />
		
	<cfset DateArray = [dateFormat(now(), "short"), dateFormat(dateadd('d',1,now()), "short"), dateFormat(dateadd('d',2,now()), "short")] />
		
	<cfdump var = "#DateArray#" />
		
	<cfset DateStruct = { today=dateFormat(now(), "short"), tomorrow=dateFormat(dateadd('d',1,now()), "short"), After_Tomorrow=dateFormat(dateadd('d',2,now()), "short") } />
		
	<cfdump var = "#DateStruct#" /><br><hr>
	
	
	
	

<cfset ThingsILike = ["Warm Sandy Beaches", "Tropical Drinks", 42] />
	<cfdump var = "#ThingsILike#" />	<br><hr>



<cfset ThingsILike = arrayNew(1) />
	<cfset ArrayAppend( ThingsILike,  "Tropical Drinks") />
	<cfset ArrayAppend( ThingsILike,  42) />
	<cfset ArrayAppend( ThingsILike,  "Grasslands") />
	<cfset ArrayAppend( ThingsILike,  84) />	
	<cfdump var="#ThingsILike#" /><br><hr>




<cfset ImportantDates = ["12/26/1975", now() ] />
	<cfdump var = "#ImportantDates#" /><br><hr>





<cfset Session.BodyType = "Perfect">
	<cfoutput>
	  Body Type: #Session.BodyType#
	</cfoutput><br><hr>



<cfset fruitBasket = {
    "Apple" = "Like",
    "Banana" = "Like",
    "Cherry" = "Dislike"
} />
	<cfdump var = "#FruitBasket#" />
	
	
	

<cfloop collection="#fruitBasket#" item="fruit">
    <cfoutput>I #fruitBasket[fruit]# #fruit#</cfoutput><br />
</cfloop>




<cfset fruitBasket = {
    "One" = 1 ,
    "Two" = 2 ,
    "Three" = 3
} />
	<cfdump var = "#fruitBasket#" />
	
	
	

<cfloop collection="#fruitBasket#" item="fruit">
    <cfoutput>I #fruitBasket[fruit]# #fruit#</cfoutput><br />
</cfloop><br><hr>





<h2>List Loop 1</h2>
	<cfset numlist="1,2,3,4,5">
		<cfloop index="num" list="#numlist#">
			<cfoutput>#num#</cfoutput>
		</cfloop>



<h2>List Loop 2</h2>
	<cfset beatles="paul john ringo george">
		<ul>
			<cfloop index="beatle" list="#beatles#" delimiters=" ">
				<li><cfoutput>#beatle#</cfoutput></li>
			</cfloop>
		</ul>





<html>
<head>
  <title>Running Log</title>
  <style type="text/css">
  	.grayBg {background-color:#cccccc; font-style:italic;}
	.whiteBg {background-color:#ffffff; font-weight:bold}
  </style>
</head>
<body>
<h1>Running Log</h1>
<a href="AddEntry.cfm">Add Entry</a><hr/>
<cfset RunningLogPath = ExpandPath("Logs/RunningLog.txt")>
<cfset CrLf = chr(10) & chr(13)>
<cfif FileExists(RunningLogPath)>
	<cffile action="read" file="#RunningLogPath#" variable="myfile">
	<table border="1" bgcolor="#ccccff">
	<tr>
		<th>Date</th>
		<th>Distance</th>
		<th>Time</th>
		<th>Comments</th>
	</tr>
	<cfloop list="#myfile#" index="run" delimiters="#CrLf#">
		<cfif NOT isDefined("bg") OR bg EQ "grayBg">
			<cfset bg="whiteBg">
		<cfelse>
			<cfset bg="grayBg">
		</cfif>
	
		<cfoutput>
			<tr class="#bg#">
			<cfloop list="#run#" index="col" delimiters="#chr(9)#">
				<td>#col#</td>
			</cfloop>
			</tr>
		</cfoutput>
	</cfloop>
	</table>
<cfelse>
	You have apparently never been running.
</cfif>

</body>
</html>
<hr>



<cfset beatles = ArrayNew(1)>
	<cfset ArrayAppend(beatles,"John")>
	<cfset ArrayAppend(beatles,"Paul")>
	<cfset ArrayAppend(beatles,"George")>
	<cfset ArrayAppend(beatles,"Ringo")>

		<!---output a particular index to the browser--->
		<cfoutput>#beatles[3]#</cfoutput>

<ul>
<cfloop from="1" to="#ArrayLen(beatles)#" index="i">
	<li><cfoutput>#beatles[i]#</cfoutput></li>
</cfloop>
</ul>



<cfset rockBands = ArrayNew(2) >
	<cfset rockBands[1][1] = "Faridabad">
	<cfset rockBands[1][2] = "Delhi">
	<cfset rockBands[1][3] = "Noida">

	<cfset rockBands[2][1] = "Pulkit">
	<cfset rockBands[2][2] = "John">
	<cfset rockBands[2][3] = "Baren">

	<cfset rockBands[3][1] = "Rahul">
	<cfset rockBands[3][2] = "Jame">
	<cfset rockBands[3][3] = "Ray">
	

<table border="1">
	<cfloop from="1" to="#ArrayLen(rockbands[1])#" index="i">
	<tr>
		<cfoutput>
			<cfloop from="1" to="#ArrayLen(rockbands[i])#" index="j">
				<th>#rockbands[i][j]#</th>
			</cfloop>
		</cfoutput>
	</tr>
	</cfloop>
</table>


<hr>


<cfset name="Rahul">
	<cfif name eq "Pulkit">
		<cfoutput>
			Hello #name#
		</cfoutput>
	<cfelseif name eq "Rahul">
		<cfoutput>
			Hello #name#!
		</cfoutput>
	<cfelse>
			Hello Please Verify
	</cfif><hr>




<cfset myVar="False">
<cfloop condition="myVar eq false">
	<cfoutput>
		myVar is in Loop <br>
	</cfoutput>	
		
	<cfif RandRange(1,10) eq 10>
		<cfset myVar="true">
	</cfif>
 </cfloop>

	<cfoutput>
	my Var is out of loop!!!!!!!!
	</cfoutput>

<hr>

<cfloop list="ColdFusion,  HTML" index="ListItem" delimiters=" , ">
 	<cfoutput>
   		#ListItem#<br />
	</cfoutput>
</cfloop><hr>




<cfset myFile="Hello this is Pulkit How may i help you">
<cfloop list="#myFile#" index="FileItem" >
  <cfoutput>
  	 #FileItem#<br />
 </cfoutput>
</cfloop><hr>




<cfset myStruct = StructNew()>
	<cfset StructInsert(myStruct, "Coldfusion","Coldfusion MX Bible")>
	<cfset StructInsert(myStruct, "HTML","Html visual Quickstart")>
	<cfset StructInsert(mystruct, "XML","Inside  XML")>

	<cfloop collection="#myStruct#" item="my">
		<cfoutput>
			#my#: #StructFind(myStruct,my)#<br>
		</cfoutput>
	</cfloop>


<hr>
<cfset mySkills = "coldfusion,html,css,php"/>
<div class="skills">
	<ul>
		<cfoutput>
			<cfloop list="#mySkills#" index="skill">
				<li>#skill#</li>
			</cfloop>	
		</cfoutput>
	</ul>
</div>




<cfset fullName = getFullName(firstName="Emily", lastName="Christiansen") />

<cfoutput>
    Hello, #fullName#!
</cfoutput>

<cffunction name="getFullName" output="false" access="public" returnType="string">
    <cfargument name="firstName" type="string" required="false" default="" />
    <cfargument name="lastName" type="string" required="false" default="" />

    <cfset var fullName = arguments.firstName & " " & arguments.lastname />

    <cfreturn fullName />
</cffunction>






<cfdump var = "#1 + 2#" /><br />

<cfset DateToday = "Today is: " & now() />
<cfdump var = "#DateToday#" />


	





