<cfdocument format="PDF">
	<cfoutput>
		<!--- Section 1 --->
		<cfdocumentsection name="bookmark1">
		    <cfdocumentitem type="header">
		    <h1 style="text-align:center;">BIG FANCY HEADER</h1>
		    </cfdocumentitem>
		    Bacon ipsum dolor sit amet sirloin fatback #dateformat(now(), "short")#
		    <cfdocumentitem type="footer">
		    <h1 style="text-align:center;">Page #cfdocument.currentPageNumber# of #cfdocument.totalPageCount#</h1>
		    </cfdocumentitem>
		</cfdocumentsection>
		
		<!--- Section 2 --->
		<cfdocumentsection name="bookmark2">
		    <cfdocumentitem type="header">
		    <h1 style="text-align:center;">2nd page header</h1>
		    </cfdocumentitem>
		    Bacon ipsum dolor sit amet sirloin fatback #dateformat(now(), "short")#
		    <cfdocumentitem type="footer">
		    <h1 style="text-align:center;">Page #cfdocument.currentPageNumber# of #cfdocument.totalPageCount#</h1>
		    </cfdocumentitem>
		</cfdocumentsection>
	</cfoutput>
</cfdocument>